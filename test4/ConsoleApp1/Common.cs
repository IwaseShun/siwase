﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static System.Console;

namespace ConsoleApp1
{
    static class Common
    {
        /// <summary>
        /// 使用メモリを表示する（MB）
        /// </summary>
        /// <param name="str"></param>
        static void WriteMemory(string str)
        {
            var total = GC.GetTotalMemory(true) / 1024.0 / 1024.0;
            WriteLine($"{str}: {total:0.0MB}");
        }
    }
}
