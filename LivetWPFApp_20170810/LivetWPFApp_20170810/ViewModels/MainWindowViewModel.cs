﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.ComponentModel;
using System.Diagnostics;

using Livet;
using Livet.Commands;
using Livet.Messaging;
using Livet.Messaging.IO;
using Livet.EventListeners;
using Livet.Messaging.Windows;

using LivetWPFApp_20170810.Models;
using System.Windows;

namespace LivetWPFApp_20170810.ViewModels
{
    public class MainWindowViewModel : ViewModel
    {
        #region メンバ

        Model model = new Model();

        /// <summary>
        /// C# 6 以降で設定可能な初期化子（参考URL http://ufcpp.net/study/csharp/ap_ver6.html#auto-property）
        /// </summary>
        public ObservableCollection<OneLine> List { get; set; } = new ObservableCollection<OneLine>();


        #endregion

        /// <summary>
        /// 既存の初期化処理
        /// </summary>
        public void Initialize()
        {
        }

        /// <summary>
        /// コンストラクタ
        /// </summary>
        public MainWindowViewModel()
        {
            // ここでファイルの読込などを行う想定
            List.Add(new OneLine { UserChecked = false, UserName = "セブンイレブン", UserNo = "1" });
            List.Add(new OneLine { UserChecked = false, UserName = "サンクス", UserNo = "2" });
            List.Add(new OneLine { UserChecked = true, UserName = "ファミリーマート", UserNo = "3" });
            List.Add(new OneLine { UserChecked = false, UserName = "ローソン", UserNo = "4" });
            List.Add(new OneLine { UserChecked = false, UserName = "sony", UserNo = "5" });

            // 検証1
            var listener = new PropertyChangedEventListener(model);
            listener.RegisterHandler((sender, e) => PropertyUpdateHandler(sender, e));
            this.CompositeDisposable.Add(listener);

            // 検証2
            //var listener = new PropertyChangedEventListener(List);
            //listener.RegisterHandler((sender, e) => PropertyUpdateHandler(sender, e));
            //this.CompositeDisposable.Add(listener);

        }

        /// <summary>
        /// 更新イベント発行（スケルトンまでの作成）
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void PropertyUpdateHandler(object sender, PropertyChangedEventArgs e)
        {
            Debug.Print("e.PropertyName : " + e.PropertyName);
            var targetModel = sender as Model;
            switch (e.PropertyName)
            {
                default:
                    RaisePropertyChanged("");
                    break;
            }
        }

        /// <summary>
        /// ListViewに表示する行データ
        /// </summary>
        public class OneLine
        {
            public string UserName { get; set; }
            public string UserNo { get; set; }
            public bool UserChecked { get; set; }
        }
    }
}
