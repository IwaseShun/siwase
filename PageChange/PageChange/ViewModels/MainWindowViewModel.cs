﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;

using Livet;
using Livet.Commands;
using Livet.Messaging;
using Livet.Messaging.IO;
using Livet.EventListeners;
using Livet.Messaging.Windows;

using PageChange.Models;
using System.Windows.Input;

namespace PageChange.ViewModels
{
    public class MainWindowViewModel : INotifyPropertyChanged
    {
        /* コマンド、プロパティの定義にはそれぞれ 
         * 
         *  lvcom   : ViewModelCommand
         *  lvcomn  : ViewModelCommand(CanExecute無)
         *  llcom   : ListenerCommand(パラメータ有のコマンド)
         *  llcomn  : ListenerCommand(パラメータ有のコマンド・CanExecute無)
         *  lprop   : 変更通知プロパティ(.NET4.5ではlpropn)
         *  
         * を使用してください。
         * 
         * Modelが十分にリッチであるならコマンドにこだわる必要はありません。
         * View側のコードビハインドを使用しないMVVMパターンの実装を行う場合でも、ViewModelにメソッドを定義し、
         * LivetCallMethodActionなどから直接メソッドを呼び出してください。
         * 
         * ViewModelのコマンドを呼び出せるLivetのすべてのビヘイビア・トリガー・アクションは
         * 同様に直接ViewModelのメソッドを呼び出し可能です。
         */

        /* ViewModelからViewを操作したい場合は、View側のコードビハインド無で処理を行いたい場合は
         * Messengerプロパティからメッセージ(各種InteractionMessage)を発信する事を検討してください。
         */

        /* Modelからの変更通知などの各種イベントを受け取る場合は、PropertyChangedEventListenerや
         * CollectionChangedEventListenerを使うと便利です。各種ListenerはViewModelに定義されている
         * CompositeDisposableプロパティ(LivetCompositeDisposable型)に格納しておく事でイベント解放を容易に行えます。
         * 
         * ReactiveExtensionsなどを併用する場合は、ReactiveExtensionsのCompositeDisposableを
         * ViewModelのCompositeDisposableプロパティに格納しておくのを推奨します。
         * 
         * LivetのWindowテンプレートではViewのウィンドウが閉じる際にDataContextDisposeActionが動作するようになっており、
         * ViewModelのDisposeが呼ばれCompositeDisposableプロパティに格納されたすべてのIDisposable型のインスタンスが解放されます。
         * 
         * ViewModelを使いまわしたい時などは、ViewからDataContextDisposeActionを取り除くか、発動のタイミングをずらす事で対応可能です。
         */

        /* UIDispatcherを操作する場合は、DispatcherHelperのメソッドを操作してください。
         * UIDispatcher自体はApp.xaml.csでインスタンスを確保してあります。
         * 
         * LivetのViewModelではプロパティ変更通知(RaisePropertyChanged)やDispatcherCollectionを使ったコレクション変更通知は
         * 自動的にUIDispatcher上での通知に変換されます。変更通知に際してUIDispatcherを操作する必要はありません。
         */

        // StepAViewModelインスタンスまたはStepBViewModelインスタンス保持用
        private Dictionary<string, HyoujiViewModelBase> _HyoujiViewModels = new Dictionary<string, HyoujiViewModelBase>();


        public MainWindowViewModel()
        {
        }

        #region INotifyPropertyChanged を実装
        public event PropertyChangedEventHandler PropertyChanged;
        #endregion

        #region Viewへの通知ヘルパー  各プロパティセッターはこれを呼べばよい
        protected void RaisePropertyChanged(string propertyName)
        {
            var d = PropertyChanged;
            if (d != null)  // バインドされていたら
            {
                d(this, new PropertyChangedEventArgs(propertyName));    // 変化を通知
            }
        }
        #endregion


        // MainWindow->ContentControl->Content にバインドされるプロパティ
        private HyoujiViewModelBase _CurrentStepViewModel;
        public HyoujiViewModelBase CurrentStepViewModel
        {
            get
            {
                if (_CurrentStepViewModel == null)
                {
                    // アプリ起動時など
                    // あえて null を返すと何も表示されない
                    // _CurrentStepViewModel = GetOrCreateHyoujiViewModel("PageChange.ViewModels.HyoujiAViewModel");
                }
                return _CurrentStepViewModel;
            }
            set
            {
                if (_CurrentStepViewModel != value)
                {
                    _CurrentStepViewModel = value;
                    RaisePropertyChanged("CurrentStepViewModel");
                    (ChangeViewCommand as DelegateCommand).RaiseCanExecuteChanged();
                    (ChangeViewCommand as DelegateCommand).RaiseCanExecuteChanged();
                }
            }
        }


        /// <summary>
        /// 各画面のViewModelインスタンスを返す (作るかまたは再利用) 
        /// </summary>
        /// <param name="viewmodel_name"></param>
        /// <returns></returns>
        private HyoujiViewModelBase GetOrCreateHyoujiViewModel(string viewmodel_name)
        {
            HyoujiViewModelBase view_model = null;
            if (!_HyoujiViewModels.TryGetValue(viewmodel_name, out view_model))
            {
                Type t = Type.GetType(viewmodel_name);
                object obj = t.InvokeMember(null,
                        System.Reflection.BindingFlags.CreateInstance,
                        null, null, null);
                view_model = obj as HyoujiViewModelBase;
                _HyoujiViewModels[viewmodel_name] = view_model;
            }
            return view_model;
        }


        /// <summary>
        /// コマンド実行のための コマンド名+"Execute" メソッド
        /// 実行可能判定のための コマンド名+"CanExecute" メソッド
        /// が存在するという自分ルール前提でデリゲートを生成
        /// </summary>
        /// <param name="command_name"></param>
        /// <returns></returns>
        private DelegateCommand CreateDelegateCommandInstance(string command_name)
        {
            var execute_handler = Delegate.CreateDelegate(
                    typeof(Action<object>),
                    this,
                    command_name + "Execute"
                ) as Action<object>;

            var can_execute_handler = Delegate.CreateDelegate(
                    typeof(Func<object, bool>),
                    this,
                    command_name + "CanExecute"
                )
                as Func<object, bool>;

            var res = new DelegateCommand
            {
                ExecuteHandler = execute_handler,
                CanExecuteHandler = can_execute_handler
            };
            return res;
        }


        //  View変更コマンド
        private ICommand _ChangeViewCommand;
        public ICommand ChangeViewCommand
        {
            get
            {
                if (_ChangeViewCommand == null)
                    _ChangeViewCommand = CreateDelegateCommandInstance("ChangeViewCommand");
                return _ChangeViewCommand;
            }
        }
        private void ChangeViewCommandExecute(object parameter)
        {
            CurrentStepViewModel = GetOrCreateHyoujiViewModel(String.Format("PageChange.ViewModels.{0}ViewModel", parameter as string));
        }
        private bool ChangeViewCommandCanExecute(object parameter)
        {
            return _CurrentStepViewModel == null ? true :
                    _CurrentStepViewModel.GetType().Name != String.Format("{0}ViewModel", parameter as string);
        }
        public void Initialize()
        {
        }
    }
}
