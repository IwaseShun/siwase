﻿using System;
using System.Windows;
using System.Windows.Forms;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;
using static System.Console;

namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {
            // 確認用
            //検証1();

            // 検証用2
            //testLinq_1();

            // 検証用3 ※コーディングミスがあるので見つけてみよう！
            // コーディングミス（$の前に「"」が記載されているため「header」も文字として判定されてしまう）
            // WriteLineとWriteの改行の違い
            testLinq_2();

            // オリジナル検証
            testLinq_3();

            // オリジナル検証2
            testLinq_4();

            // オリジナル検証3
            testLinq_5();

            // オリジナル検証4
            testLinq_6();

            Console.ReadKey();
        }

        #region おりたたみサンプル
        /// <summary>
        /// 日本語も呼べますよ。（普通は使わない）
        /// </summary>
        static void 検証1()
        {
            MessageBox.Show("熱い");

            Debug.WriteLine("aaaaaaaa");

        }
        #endregion


        /// <summary>
        /// リンクサンプル1
        /// </summary>
        /// <returns></returns>
        static bool testLinq_1()
        {

            IEnumerable<int> numbers = Enumerable.Range(1, 10);

            // 合計値の計算
            int sum = numbers.Sum();

            // 最小値の計算
            int min = numbers.Min();

            // 最大値の計算
            int max = numbers.Max();

            // 平均値の計算
            double avg = numbers.Average();

            WriteLine("合計は「{0}」です。", sum);

            WriteLine($"合計値は「{sum}」です。");
            WriteLine($"最小値は「{min}」です。");
            WriteLine($"最大値は「{max}」です。");
            WriteLine($"平均値は「{avg}」です。");

            return false;

        }

        /// <summary>
        /// リンクサンプル2
        /// </summary>
        /// <returns></returns>
        static bool testLinq_2()
        {

            IEnumerable<int> numbers = Enumerable.Range(0, 55);

            var results = new List<int>();
            foreach(int n in numbers)
            {
                if (n % 2 ==0)
                {
                    results.Add(n);
                }
            }
            WriteNumbers(results, "【偶数：old実装】");


            var ret = numbers.Where(n => n % 2 == 0);
            WriteNumbers(results, "【偶数：new実装】");

            return false;

        }

        /// <summary>
        /// オリジナルサンプル
        /// </summary>
        /// <returns></returns>

        static bool testLinq_3()
        {
            IEnumerable<int> numbers = Enumerable.Range(0, 100);

            var results = numbers.Where(n => n % 2 != 0);
            WriteNumbers_Kisuu(results, "【奇数】");

            return false;
        }

        /// <summary>
        /// オリジナルサンプル2
        /// </summary>
        /// <returns></returns>

        static bool testLinq_4()
        {
            IEnumerable<int> numbers = Enumerable.Range(0, 100);

            // 数字が0から始まるため、0以外の条件を記載
            var results = numbers.Where(n => n % 3 == 0 && n != 0);
            WriteNumbers_3Bai(results, "【3の倍数】");

            return false;
        }

        /// <summary>
        /// オリジナルサンプル3
        /// </summary>
        /// <returns></returns>

        static bool testLinq_5()
        {
            IEnumerable<int> numbers = Enumerable.Range(0, 100);

            // 数字が0から始まるため、0以外の条件を記載
            var results = numbers.Where(n => n % 7 == 0 && n != 0);
            WriteNumbers_3Bai(results, "【7の倍数】");

            return false;
        }

        static bool testLinq_6()
        {
            IEnumerable<int> numbers = Enumerable.Range(0, 10000);

            double result = numbers.Average();
            WriteNumbers_Avarage(result , "【平均値】");

            return false;
        }

        /// <summary>
        /// コレクション内のすべての整数を表示するメソッド／C#6から
        /// </summary>
        /// <param name="numbers"></param>
        /// <param name="header"></param>
        static void WriteNumbers(IEnumerable<int> numbers, string header)
        {
            Write($"{header}:");
            foreach (var n in numbers)
                Write($" {n}");
            WriteLine();
        }

        /// <summary>
        /// コレクション内のすべての整数を表示するメソッド／従来
        /// </summary>
        /// <param name="numbers"></param>
        /// <param name="header"></param>
        static void WriteNumbers_olstyle(IEnumerable<int> numbers, string header)
        {
            Write("{0}:", header);
            foreach (var n in numbers)
                Write(" {0}", n);
            WriteLine();
        }

        /// <summary>
        /// コレクション内のすべての奇数を表示するメソッド／オリジナル
        /// </summary>
        /// <param name="numbers"></param>
        /// <param name="header"></param>
        static void WriteNumbers_Kisuu(IEnumerable<int> numbers, string header)
        {
            Write($"{header}:");
            foreach (var n in numbers)
                Write($" {n}");
            WriteLine();
        }

        /// <summary>
        /// コレクション内のすべての3の倍数を表示するメソッド／オリジナル
        /// </summary>
        /// <param name="numbers"></param>
        /// <param name="header"></param>
        static void WriteNumbers_3Bai(IEnumerable<int> numbers, string header)
        {
            Write($"{header}:");
            foreach (var n in numbers)
                Write($" {n}");
            WriteLine();
        }

        /// <summary>
        /// コレクション内のすべての7の倍数を表示するメソッド／オリジナル
        /// </summary>
        /// <param name="numbers"></param>
        /// <param name="header"></param>
        static void WriteNumbers_7Bai(IEnumerable<int> numbers, string header)
        {
            Write($"{header}:");
            foreach (var n in numbers)
                Write($" {n}");
            WriteLine();
        }

        /// <summary>
        /// コレクション内の平均値を表示するメソッド／オリジナル
        /// </summary>
        /// <param name="numbers"></param>
        /// <param name="header"></param>
        static void WriteNumbers_Avarage(double numbers, string header)
        {
            Write($"{header}:");
            WriteLine(numbers);
        }
    }
}
